package facci.example.faccipmchavezbailonsqlite;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText imei, gama, marca, modelo, companiatelefonica, id;
    private Button guardar, leerT, actualizar, eliminarT;
    private TextView Datos;
    private DataBase dataBase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

         imei= (EditText)findViewById(R.id.imei);
        gama = (EditText)findViewById(R.id.gama);
        marca = (EditText)findViewById(R.id.marca);
        modelo = (EditText)findViewById(R.id.modelo);
        companiatelefonica = (EditText)findViewById(R.id.companiatelefonica);
        id = (EditText)findViewById(R.id.Id);
        guardar = (Button)findViewById(R.id.Guardar);
        leerT = (Button)findViewById(R.id.LeerT);
        actualizar = (Button)findViewById(R.id.Actualizar);
        eliminarT = (Button)findViewById(R.id.EliminarT);

        guardar.setOnClickListener(this);
        leerT.setOnClickListener(this);
        actualizar.setOnClickListener(this);
        eliminarT.setOnClickListener(this);
        Datos = (TextView)findViewById(R.id.Datos);

        dataBase = new DataBase(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.Guardar:

                if (imei.getText().toString().isEmpty()){

                }else if(imei.getText().toString().isEmpty()){

                }else if(gama.getText().toString().isEmpty()){

                }else if (marca.getText().toString().isEmpty()){

                }else if(modelo.getText().toString().isEmpty()){

                }else if(companiatelefonica.getText().toString().isEmpty()){

                }else {
                    dataBase.Insertar(imei.getText().toString(), gama.getText().toString(),
                            marca.getText().toString(), modelo.getText().toString(), companiatelefonica.getText().toString());
                    Toast.makeText(this, "GUARDADO", Toast.LENGTH_SHORT).show();
                    id.setText("");
                    imei.setText("");
                    gama.setText("");
                    marca.setText("");
                    modelo.setText("");
                    companiatelefonica.setText("");
                    Datos.setText("");
                }

                break;

            case R.id.LeerT:
                Datos.setText(dataBase.LeerTodos());
                id.setText("");
                imei.setText("");
                gama.setText("");
                marca.setText("");
                modelo.setText("");
                companiatelefonica.setText("");
                Toast.makeText(this, "LISTADOS", Toast.LENGTH_SHORT).show();
                break;


            case R.id.Actualizar:
                if (imei.getText().toString().isEmpty()){

                }else if(imei.getText().toString().isEmpty()){

                }else if(gama.getText().toString().isEmpty()){

                }else if (marca.getText().toString().isEmpty()){

                }else if(modelo.getText().toString().isEmpty()){

                }else if(companiatelefonica.getText().toString().isEmpty()){

                }else if(id.getText().toString().isEmpty()){

                }else {
                    dataBase.Actualizar(id.getText().toString(), imei.getText().toString(), gama.getText().toString(),
                            marca.getText().toString(), modelo.getText().toString(), companiatelefonica.getText().toString());
                    Toast.makeText(this, "ACTUALIZADO", Toast.LENGTH_SHORT).show();
                    id.setText("");
                    imei.setText("");
                    gama.setText("");
                    marca.setText("");
                    modelo.setText("");
                    companiatelefonica.setText("");
                    Datos.setText("");
                }
                break;

            case R.id.EliminarT:
                dataBase.EliminarTodo();
                Datos.setText("");
                Toast.makeText(this, "ELIMINADOS", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
