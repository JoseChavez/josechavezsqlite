package facci.example.faccipmchavezbailonsqlite;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;

public class DataBase extends SQLiteOpenHelper {


    private static int version = 1;
    private static String name = "Prueba1" ;
    private static SQLiteDatabase.CursorFactory factory = null;

    public DataBase(Context context) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE Universidades(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "imei VARCHAR," +
                "gama VARCHAR," +
                "marca VARCHAR," +
                "modelo VARCHAR, " +
                "companiatelefonica VARCHAR)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS Universidades";
        db.execSQL(sql);
        onCreate(db);
    }


    public void Insertar(String imei, String gama, String marca, String modelo, String companiatelefonica){

        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("imei", imei);
        contentValues.put("gama", gama);
        contentValues.put("marca", marca);
        contentValues.put("modelo", modelo);
        contentValues.put("companiatelefonica", companiatelefonica);
        database.insert("Universidades",null,contentValues);
    }


    public void EliminarTodo(){
        SQLiteDatabase database = this.getWritableDatabase();
        database.delete("Universidades", null, null);
    }

    public void Actualizar(String id, String imei, String gama, String marca, String modelo, String companiatelefonica){
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("imei", imei);
        contentValues.put("gama", gama);
        contentValues.put("marca", marca);
        contentValues.put("modelo", modelo);
        contentValues.put("companiatelefonica", companiatelefonica);
        database.update("Universidades", contentValues, "id= " + id, null);
    }



    public String LeerTodos() {
        SQLiteDatabase database = this.getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        String result = "";
        String[] sqlSelect = {"id","imei", "gama", "marca", "modelo", "companiatelefonica"};
        String sqlTable = "Universidades";
        qb.setTables(sqlTable);
        Cursor c = qb.query(database, sqlSelect, null, null, null, null, null);
        if (c.moveToFirst()) {
            do {
                result += "id " + c.getInt(c.getColumnIndex("id")) + "\n" +
                        "imei " + c.getString(c.getColumnIndex("imei")) + "\n" +
                        "gama " + c.getString(c.getColumnIndex("gama")) + "\n" +
                        "marca " + c.getString(c.getColumnIndex("marca")) + "\n" +
                        "modelo " + c.getString(c.getColumnIndex("modelo")) + "\n" +
                        "companiatelefonica " + c.getString(c.getColumnIndex("companiatelefonica")) + "\n\n\n";
            } while (c.moveToNext());
            c.close();
            database.close();
        }
        return result;

    }

}
